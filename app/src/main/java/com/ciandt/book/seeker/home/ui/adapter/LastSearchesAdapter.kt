package com.ciandt.book.seeker.home.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.ciandt.book.seeker.R
import com.ciandt.book.seeker.shared.persistence.domain.model.LastSearch


class LastSearchesAdapter(
    context: Context,
    private val onItemCLick: (lastSearch: LastSearch) -> Unit
) : AdapterWithHeader<LastSearch, RecyclerView.ViewHolder>(ITEM_COMPARATOR) {

    companion object {
        private const val HEADER_TYPE = 0
        private const val ITEM_TYPE = 1

        // A DiffUtil.ItemCallback for calculating the diff between two non-null items in a list.
        val ITEM_COMPARATOR = object : DiffUtil.ItemCallback<LastSearch>() {
            override fun areContentsTheSame(oldItem: LastSearch, newItem: LastSearch): Boolean {
                return oldItem == newItem
            }

            override fun areItemsTheSame(oldItem: LastSearch, newItem: LastSearch): Boolean {
                return oldItem.query == newItem.query
            }
        }
    }

    private val layoutInflater = LayoutInflater.from(context)

    var items: List<LastSearch> = emptyList()
        set(value) {
            submitList(value)
            field = value
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            HEADER_TYPE -> HeaderViewHolder(
                layoutInflater.inflate(
                    R.layout.header_item,
                    parent,
                    false
                )
            )
            ITEM_TYPE -> LastSearchViewHolder(
                layoutInflater.inflate(
                    R.layout.last_search_item,
                    parent,
                    false
                )
            )
            else -> throw IllegalStateException("Unexpected viewType: $viewType")
        }
    }

    override fun getItemCount() = items.size + 1

    override fun getItemViewType(position: Int) =
        when (position) {
            0 -> HEADER_TYPE
            else -> ITEM_TYPE
        }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is LastSearchViewHolder -> {
                val item = items[position - 1]
                holder.run {
                    lastSearch = item
                    itemView.setOnClickListener { onItemCLick.invoke(item) }
                }
            }
        }
    }

    class LastSearchViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val lastSearchQuery = itemView.findViewById<TextView>(R.id.last_search_query)

        var lastSearch: LastSearch? = null
            set(value) {
                field = value

                lastSearchQuery.text = value?.query
            }
    }

    class HeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.findViewById<TextView>(R.id.last_search_query).text =
                itemView.context.getString(R.string.last_searches_header)
        }
    }
}