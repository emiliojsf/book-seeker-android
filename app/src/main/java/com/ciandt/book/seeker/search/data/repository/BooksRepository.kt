package com.ciandt.book.seeker.search.data.repository

import com.ciandt.book.seeker.search.data.source.SearchRemoteDataSource
import com.ciandt.book.seeker.search.domain.repository.IBooksRepository
import com.ciandt.book.seeker.shared.common.model.Book
import com.ciandt.book.seeker.shared.network.domain.model.Result

class BooksRepository(private val dataSource: SearchRemoteDataSource) : IBooksRepository {

    private val booksCache = mutableMapOf<Long, Book>()

    override suspend fun search(query: String): List<Book>? {
        val result = dataSource.search(query)
        return if (result is Result.Success) {
            cache(result.data)
            result.data
        } else {
            null
        }
    }

    override fun getBook(id: Long) = booksCache[id]

    private fun cache(books: List<Book>) {
        books.associateByTo(booksCache) { it.trackId }
    }
}