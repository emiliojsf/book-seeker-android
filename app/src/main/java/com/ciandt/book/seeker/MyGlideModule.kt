package com.ciandt.book.seeker

import android.app.ActivityManager
import android.content.Context
import android.content.Context.ACTIVITY_SERVICE
import com.bumptech.glide.GlideBuilder
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.module.AppGlideModule
import com.bumptech.glide.request.RequestOptions


@GlideModule
class MyGlideModule : AppGlideModule() {
    override fun applyOptions(context: Context, builder: GlideBuilder) {
        val defaultOptions = RequestOptions()
        val activityManager = context.getSystemService(ACTIVITY_SERVICE) as ActivityManager

        builder.setDefaultRequestOptions(defaultOptions.apply {
            // Prefer higher quality images unless we're on a low RAM device
            format(if (activityManager.isLowRamDevice) DecodeFormat.PREFER_RGB_565 else DecodeFormat.PREFER_ARGB_8888)
            // Disable hardware bitmaps as they don't play nicely with Palette
            disallowHardwareConfig()
        })
    }

    override fun isManifestParsingEnabled(): Boolean {
        return false
    }
}