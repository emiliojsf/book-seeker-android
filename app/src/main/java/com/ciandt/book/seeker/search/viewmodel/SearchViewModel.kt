package com.ciandt.book.seeker.search.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ciandt.book.seeker.search.domain.repository.IBooksRepository
import com.ciandt.book.seeker.shared.common.model.Book
import com.ciandt.book.seeker.shared.persistence.domain.repository.ILastSearchRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SearchViewModel(
    private val booksRepository: IBooksRepository,
    private val lastSearchDatabaseRepository: ILastSearchRepository
) : ViewModel() {

    private val scope = CoroutineScope(Dispatchers.IO)

    val searchResults = MutableLiveData<List<Book>>()

    fun searchFor(query: String) {
        scope.launch {
            lastSearchDatabaseRepository.insertOrUpdate(query)
            searchResults.postValue(booksRepository.search(query))
        }
    }
}