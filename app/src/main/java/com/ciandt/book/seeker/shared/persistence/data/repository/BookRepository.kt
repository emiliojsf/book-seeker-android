package com.ciandt.book.seeker.shared.persistence.data.repository

import com.ciandt.book.seeker.shared.persistence.domain.repository.IBookRepository
import com.ciandt.book.seeker.shared.persistence.domain.dao.BookDao
import com.ciandt.book.seeker.shared.persistence.domain.model.Book
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.*

class BookRepository(private val bookDao: BookDao) : IBookRepository {

    companion object {
        private const val BOOKS_LIMIT_IN_DATABASE = 10
    }

    override suspend fun insertOrUpdate(book: Book) = withContext(Dispatchers.IO) {
        bookDao.insert(book.apply { updatedAt = Date() })
        deleteExcessItems()
    }

    override suspend fun getAllBooks(): List<Book> = withContext(Dispatchers.IO) {
        return@withContext getAllItemsSorted()
    }

    private suspend fun getAllItemsSorted() = withContext(Dispatchers.IO) {
        return@withContext bookDao.getAll().sortedByDescending { it.updatedAt }
    }

    private suspend fun deleteExcessItems() = withContext(Dispatchers.IO) {
        val excess = getAllItemsSorted().drop(BOOKS_LIMIT_IN_DATABASE)
        bookDao.delete(excess.map { it.id })
    }
}