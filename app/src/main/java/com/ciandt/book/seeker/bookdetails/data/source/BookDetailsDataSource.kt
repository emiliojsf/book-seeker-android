package com.ciandt.book.seeker.bookdetails.data.source

import com.ciandt.book.seeker.shared.network.remote.MyApi
import com.ciandt.book.seeker.shared.network.domain.model.Result
import com.ciandt.book.seeker.shared.common.model.Book
import com.ciandt.book.seeker.shared.utils.safeApiCall
import java.io.IOException


class BookDetailsDataSource(private val api: MyApi) {

    suspend fun lookup(id: Long) = safeApiCall(
        call = { requestLookup(id) },
        errorMessage = "Error getting books data"
    )

    private suspend fun requestLookup(id: Long): Result<Book> {
        val response = api.lookup(id)
        if (response.isSuccessful) {
            val body = response.body()
            if (body != null) {
                return Result.Success(body.results.first())
            }
        }
        return Result.Error(
            IOException("Error getting books data ${response.code()} ${response.message()}")
        )
    }
}