package com.ciandt.book.seeker.home.ui

import android.app.ActivityOptions
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.transition.TransitionManager
import com.ciandt.book.seeker.R
import com.ciandt.book.seeker.home.ui.adapter.BooksAdapter
import com.ciandt.book.seeker.home.ui.adapter.LastSearchesAdapter
import com.ciandt.book.seeker.home.viewmodel.HomeViewModel
import com.ciandt.book.seeker.shared.persistence.domain.model.Book
import com.ciandt.book.seeker.shared.persistence.domain.model.LastSearch
import com.ciandt.book.seeker.search.ui.SearchActivity
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.viewmodel.ext.android.viewModel


class MainActivity : AppCompatActivity() {

    private val viewModel: HomeViewModel by viewModel()
    private lateinit var booksAdapter: BooksAdapter
    private lateinit var lastSearchesAdapter: LastSearchesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupToolbar()
        setupLists()
    }

    override fun onResume() {
        super.onResume()
        viewModel.books.observe(this, Observer { books ->
            if (books.isNotEmpty()) {
                books_list.visibility = View.VISIBLE
                TransitionManager.beginDelayedTransition(container)
                booksAdapter.items = books
            } else {
                books_list.visibility = View.GONE
            }
        })

        viewModel.lastSearches.observe(this, Observer { lastSearches ->
            if (lastSearches.isNotEmpty()) {
                last_searches_list.visibility = View.VISIBLE
                TransitionManager.beginDelayedTransition(container)
                lastSearchesAdapter.items = lastSearches
            } else {
                last_searches_list.visibility = View.GONE
            }
        })

        viewModel.getAllLastSearches()
        viewModel.getAllBooks()
    }

    private fun setupToolbar() {
        toolbar.inflateMenu(R.menu.home)
        setActionBar(toolbar)
    }

    private fun setupLists() {
        booksAdapter = BooksAdapter(this@MainActivity)
        lastSearchesAdapter =
            LastSearchesAdapter(this@MainActivity, this@MainActivity::onLastSearchClicked)

        books_list.run {
            isNestedScrollingEnabled = false
            adapter = booksAdapter
            layoutManager =
                GridLayoutManager(this@MainActivity, BooksAdapter.DEFAULT_COLUMNS_NUMBER).apply {
                    spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                        override fun getSpanSize(position: Int): Int {
                            return if (booksAdapter.getItemViewType(position) == BooksAdapter.HEADER_TYPE) spanCount else 1
                        }
                    }
                }
        }

        last_searches_list.run {
            itemAnimator = DefaultItemAnimator()
            isNestedScrollingEnabled = false
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = lastSearchesAdapter
        }

    }

    private fun onLastSearchClicked(lastSearch: LastSearch) {
        goToSearchActivity(lastSearch.query)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_search -> {
                goToSearchActivity()
            }
        }

        return true
    }

    private fun goToSearchActivity(query: String? = null) {
        val searchMenuView = toolbar.findViewById<View>(R.id.menu_search)
        val options = ActivityOptions.makeSceneTransitionAnimation(
            this, searchMenuView,
            getString(R.string.transition_search_back)
        ).toBundle()

        startActivity(Intent(this, SearchActivity::class.java).apply {
            putExtra(SearchActivity.EXTRA_QUERY, query)
        }, options)
    }
}
