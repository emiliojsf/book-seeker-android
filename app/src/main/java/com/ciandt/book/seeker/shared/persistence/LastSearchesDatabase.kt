package com.ciandt.book.seeker.shared.persistence

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.ciandt.book.seeker.shared.persistence.converter.DateConverter
import com.ciandt.book.seeker.shared.persistence.domain.dao.LastSearchDao
import com.ciandt.book.seeker.shared.persistence.domain.model.LastSearch

/**
 * The Room database that contains the Last Searches table
 */
@Database(entities = [LastSearch::class], version = 1)
@TypeConverters(DateConverter::class)
abstract class LastSearchesDatabase : RoomDatabase() {

    abstract fun lastSearchDao(): LastSearchDao

}