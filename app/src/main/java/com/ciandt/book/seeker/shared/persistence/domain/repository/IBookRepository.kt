package com.ciandt.book.seeker.shared.persistence.domain.repository

import com.ciandt.book.seeker.shared.persistence.domain.model.Book

interface IBookRepository {
    suspend fun insertOrUpdate(book: Book)

    suspend fun getAllBooks(): List<Book>
}