package com.ciandt.book.seeker.bookdetails.ui

import android.os.Bundle
import android.text.Html
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.doOnPreDraw
import androidx.lifecycle.Observer
import androidx.transition.Fade
import androidx.transition.TransitionManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.ciandt.book.seeker.R
import com.ciandt.book.seeker.bookdetails.viewmodel.BookDetailsViewModel
import com.ciandt.book.seeker.extensions.DbBook
import com.ciandt.book.seeker.shared.common.model.Book
import com.ciandt.book.seeker.shared.utils.DateUtils
import kotlinx.android.synthetic.main.activity_book_details.*
import org.koin.android.viewmodel.ext.android.viewModel


class BookDetailsActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_BOOK = "extra_book"
        const val EXTRA_DB_BOOK = "extra_db_book"
    }

    private val viewModel: BookDetailsViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_book_details)
        setActionBar(toolbar)
        actionBar?.setDisplayHomeAsUpEnabled(true)
        actionBar?.title = ""
        supportPostponeEnterTransition()

        book_image.doOnPreDraw {
            supportStartPostponedEnterTransition()
        }

        viewModel.bookDetails.observe(this, Observer {
            setupBookInitialInfo(it)
        })

        viewModel.showLoading.observe(this, Observer {
            when (it) {
                true -> {
                    progress.visibility = View.VISIBLE
                    buy_button.visibility = View.GONE
                }
                false -> {
                    TransitionManager.beginDelayedTransition(button_container)
                    progress.visibility = View.GONE
                    buy_button.visibility = View.VISIBLE
                }
            }
        })

        // Get book as extra (neither Book or DbBook). If it is DbBook, we need to lookup for updated information
        setupBookInitialInfo(intent.extras?.getParcelable<Book>(EXTRA_BOOK)?.also {
            viewModel.saveBookToDatabase(it)
            buy_button.visibility = View.VISIBLE
            progress.visibility = View.GONE
        })

        setupBookInitialInfo(intent.extras?.getParcelable<DbBook>(EXTRA_DB_BOOK)?.also {
            it.id.let { id -> viewModel.getBookDetails(id) }
        })
    }

    private fun setupBookInitialInfo(book: Book?) {
        book?.let {
            book_name.text = book.trackName
            loadImage(book.artworkUrl100)
            artist_name.text = book.artistName
            description.text = stripHtml(book.description)
            buy_button.text =
                String.format(getString(R.string.button_buy_text), book.formattedPrice)
            rating.text = String.format(getString(R.string.rating_text), book.averageUserRating)
            release_date.text = String.format(
                getString(R.string.release_text),
                DateUtils.getYearFromStringDate(book.releaseDate)
            )
        }
    }

    private fun setupBookInitialInfo(book: DbBook?) {
        book?.let {
            book_name.text = book.trackName
            loadImage(book.artworkUrl100)
            artist_name.text = book.artistName
            description.text = stripHtml(book.description)
            buy_button.text =
                String.format(getString(R.string.button_buy_text), book.formattedPrice)
            rating.text = String.format(getString(R.string.rating_text), book.averageUserRating)
            release_date.text = String.format(
                getString(R.string.release_text),
                DateUtils.getYearFromStringDate(book.releaseDate)
            )
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun stripHtml(html: String?): String {
        return if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY).toString()
        } else {
            Html.fromHtml(html).toString()
        }
    }

    private fun loadImage(bookImage: String?) {
        Glide.with(this)
            .asBitmap()
            .dontAnimate()
            .fitCenter()
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .load(bookImage)
            .into(book_image)
    }
}
