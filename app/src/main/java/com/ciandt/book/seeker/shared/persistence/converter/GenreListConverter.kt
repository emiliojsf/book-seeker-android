package com.ciandt.book.seeker.shared.persistence.converter

import androidx.room.TypeConverter


class GenreListConverter {

    companion object {
        private const val GENRES_DELIMITER = ","
    }

    @TypeConverter
    fun toList(genres: String) = genres.split(GENRES_DELIMITER)


    @TypeConverter
    fun toString(genres: List<String>) = genres.joinToString { GENRES_DELIMITER }

}