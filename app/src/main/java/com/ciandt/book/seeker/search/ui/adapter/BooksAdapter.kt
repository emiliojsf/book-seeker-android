package com.ciandt.book.seeker.search.ui.adapter

import android.app.Activity
import android.app.ActivityOptions
import android.content.Intent
import android.util.Pair
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.ListPreloader
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.ciandt.book.seeker.GlideApp
import com.ciandt.book.seeker.R
import com.ciandt.book.seeker.bookdetails.ui.BookDetailsActivity
import com.ciandt.book.seeker.shared.common.model.Book

class BooksAdapter(
    private val context: Activity
) : RecyclerView.Adapter<BooksAdapter.BookViewHolder>(), ListPreloader.PreloadModelProvider<Book> {

    companion object {
        const val DEFAULT_COLUMNS_NUMBER = 2
    }

    private val layoutInflater = LayoutInflater.from(context)

    var items: List<Book> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getPreloadItems(position: Int) = listOf(items[position])

    override fun getPreloadRequestBuilder(item: Book) =
        GlideApp.with(context).load(item.artworkUrl100)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookViewHolder {
        return BookViewHolder(layoutInflater.inflate(R.layout.book_item, parent, false))
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: BookViewHolder, position: Int) {
        val item = items[position]
        holder.run {
            book = item
            itemView.setOnClickListener { view -> gotToBookDetails(view, item) }
        }
    }

    private fun gotToBookDetails(view: View?, item: Book) {
//        val options = ActivityOptions.makeSceneTransitionAnimation(
//            context,
//            Pair.create(view, context.getString(R.string.transition_book_image)),
//            Pair.create(view, context.getString(R.string.transition_book_title))
//        )

        context.startActivity(
            Intent(context, BookDetailsActivity::class.java).apply {
                putExtra(BookDetailsActivity.EXTRA_BOOK, item)
            }
//            , options.toBundle()
        )
    }

    class BookViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val context = itemView.context
        private val bookName = itemView.findViewById<TextView>(R.id.book_name)
        private val bookImage = itemView.findViewById<ImageView>(R.id.book_image)

        var book: Book? = null
            set(value) {
                field = value

                bookName.text = value?.trackName
                GlideApp.with(context).load(value?.artworkUrl100).diskCacheStrategy(
                    DiskCacheStrategy.DATA
                ).into(bookImage)
            }
    }
}