package com.ciandt.book.seeker.shared.utils

import java.text.SimpleDateFormat
import java.util.*

object DateUtils {
    fun getYearFromStringDate(date: String?): Int {
        val calendar = GregorianCalendar().apply {
            time = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'").parse(date)
        }
        return calendar.get(GregorianCalendar.YEAR)
    }
}