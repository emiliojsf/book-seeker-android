package com.ciandt.book.seeker.shared.persistence.domain.dao

import androidx.room.*
import com.ciandt.book.seeker.shared.persistence.domain.model.Book

@Dao
interface BookDao {

    /**
     * Get all books.
     * @return the books from the table.
     */
    @Query("SELECT * FROM Books")
    fun getAll(): List<Book>

    /**
     * Get a book by id.
     * @return the book from the table with a specific id.
     */
    @Query("SELECT * FROM Books WHERE bookid = :id")
    fun getBookById(id: Long): Book?

    /**
     * Insert a book in the database. If the book already exists, replace it.
     * @param book the book to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(book: Book)

    /**
     * Delete a book from the database.
     * @param idList the books to be deleted.
     */
    @Query("DELETE FROM books WHERE bookid IN (:idList)")
    fun delete(idList: List<Long>)

}