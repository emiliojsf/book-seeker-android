package com.ciandt.book.seeker.home.ui.adapter

import android.app.Activity
import android.app.ActivityOptions
import android.content.Intent
import android.util.Pair
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.ciandt.book.seeker.R
import com.ciandt.book.seeker.bookdetails.ui.BookDetailsActivity
import com.ciandt.book.seeker.shared.persistence.domain.model.Book

class BooksAdapter(
    private val context: Activity
) : AdapterWithHeader<Book, RecyclerView.ViewHolder>(ITEM_COMPARATOR) {

    companion object {
        const val DEFAULT_COLUMNS_NUMBER = 2
        const val HEADER_TYPE = 0
        const val ITEM_TYPE = 1

        // A DiffUtil.ItemCallback for calculating the diff between two non-null items in a list.
        val ITEM_COMPARATOR = object : DiffUtil.ItemCallback<Book>() {
            override fun areContentsTheSame(oldItem: Book, newItem: Book): Boolean {
                return oldItem == newItem
            }

            override fun areItemsTheSame(oldItem: Book, newItem: Book): Boolean {
                return oldItem.id == newItem.id
            }
        }
    }

    private val layoutInflater = LayoutInflater.from(context)

    var items: List<Book> = emptyList()
        set(value) {
            submitList(value)
            field = value
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            HEADER_TYPE -> HeaderViewHolder(
                layoutInflater.inflate(
                    R.layout.header_item,
                    parent,
                    false
                )
            )
            ITEM_TYPE -> BookViewHolder(layoutInflater.inflate(R.layout.book_item, parent, false))
            else -> throw IllegalStateException("Unexpected viewType: $viewType")
        }
    }

    override fun getItemCount() = items.size + 1

    override fun getItemViewType(position: Int) =
        when (position) {
            0 -> HEADER_TYPE
            else -> ITEM_TYPE
        }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is BookViewHolder -> {
                val item = items[position - 1]

                holder.run {
                    book = item
                    itemView.setOnClickListener { view ->
                        gotToBookDetails(view, item)
                    }
                }
            }
        }
    }

    private fun gotToBookDetails(view: View?, item: Book) {
//        val options = ActivityOptions.makeSceneTransitionAnimation(
//            context,
//            Pair.create(view, context.getString(R.string.transition_book_image)),
//            Pair.create(view, context.getString(R.string.transition_book_title))
//        )

        context.startActivity(
            Intent(context, BookDetailsActivity::class.java).apply {
                putExtra(BookDetailsActivity.EXTRA_DB_BOOK, item)
            }
//            , options.toBundle()
        )
    }

    class BookViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val context = itemView.context
        private val bookName = itemView.findViewById<TextView>(R.id.book_name)
        private val bookImage = itemView.findViewById<ImageView>(R.id.book_image)

        var book: Book? = null
            set(value) {
                field = value

                bookName.text = value?.trackName
                Glide.with(context)
                    .asBitmap()
                    .dontAnimate()
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .load(value?.artworkUrl100)
                    .into(bookImage)
            }
    }

    class HeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.findViewById<TextView>(R.id.last_search_query).text =
                itemView.context.getString(R.string.last_books_header)
        }
    }
}