package com.ciandt.book.seeker.search.data.source

import com.ciandt.book.seeker.shared.network.remote.MyApi
import com.ciandt.book.seeker.shared.utils.safeApiCall
import com.ciandt.book.seeker.shared.network.domain.model.Result
import com.ciandt.book.seeker.shared.common.model.Book
import java.io.IOException

class SearchRemoteDataSource(private val api: MyApi) {

    suspend fun search(query: String) = safeApiCall(
        call = { requestSearch(query) },
        errorMessage = "Error getting books data"
    )

    private suspend fun requestSearch(query: String): Result<List<Book>> {
        val response = api.search(query)
        if (response.isSuccessful) {
            val body = response.body()
            if (body != null) {
                return Result.Success(body.results)
            }
        }
        return Result.Error(
            IOException("Error getting books data ${response.code()} ${response.message()}")
        )
    }
}