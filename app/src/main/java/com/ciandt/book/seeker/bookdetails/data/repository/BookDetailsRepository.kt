package com.ciandt.book.seeker.bookdetails.data.repository

import com.ciandt.book.seeker.bookdetails.data.source.BookDetailsDataSource
import com.ciandt.book.seeker.bookdetails.domain.repository.IBookDetailsRepository
import com.ciandt.book.seeker.shared.common.model.Book
import com.ciandt.book.seeker.shared.network.domain.model.Result

class BookDetailsRepository(private val bookDetailsDataSource: BookDetailsDataSource) :
    IBookDetailsRepository {
    override suspend fun getBookDetails(id: Long) : Book? {
        val result = bookDetailsDataSource.lookup(id)
        return if(result is Result.Success) {
            result.data
        } else {
            null
        }
    }
}