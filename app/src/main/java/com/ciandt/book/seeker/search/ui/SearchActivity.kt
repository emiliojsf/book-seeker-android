package com.ciandt.book.seeker.search.ui

import android.app.SearchManager
import android.content.Context
import android.graphics.Point
import android.graphics.Typeface
import android.os.Bundle
import android.text.InputType
import android.text.style.StyleSpan
import android.transition.Transition
import android.transition.TransitionInflater
import android.transition.TransitionManager
import android.transition.TransitionSet
import android.util.SparseArray
import android.view.View
import android.view.ViewStub
import android.view.inputmethod.EditorInfo
import android.widget.SearchView
import android.widget.TextView
import androidx.annotation.TransitionRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.SharedElementCallback
import androidx.core.text.set
import androidx.core.text.toSpannable
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.integration.recyclerview.RecyclerViewPreloader
import com.bumptech.glide.util.ViewPreloadSizeProvider
import com.ciandt.book.seeker.shared.utils.ImeUtils
import com.ciandt.book.seeker.R
import com.ciandt.book.seeker.shared.utils.TransitionUtils
import com.ciandt.book.seeker.shared.common.model.Book
import com.ciandt.book.seeker.search.ui.adapter.BooksAdapter
import com.ciandt.book.seeker.search.ui.recyclerview.SlideInItemAnimator
import com.ciandt.book.seeker.search.ui.transitions.CircularReveal
import com.ciandt.book.seeker.search.viewmodel.SearchViewModel
import kotlinx.android.synthetic.main.activity_search.*
import org.koin.android.viewmodel.ext.android.viewModel

class SearchActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_QUERY = "extra_query"
    }

    private val viewModel: SearchViewModel by viewModel()
    private val transitions = SparseArray<Transition>()
    private lateinit var booksAdapter: BooksAdapter
    private var noResults: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        search_back.setOnClickListener { dismiss() }
        scrim.setOnClickListener { dismiss() }

        setupTransitions()
        booksAdapter = BooksAdapter(this)
        setupSearchView(intent.extras?.getString(EXTRA_QUERY))

        viewModel.searchResults.observe(this, Observer { books ->
            if (books.isNotEmpty()) {
                if (search_results.visibility != View.VISIBLE) {
                    TransitionManager.beginDelayedTransition(
                        container,
                        getTransition(R.transition.search_show_results)
                    )
                    progress.visibility = View.GONE
                    search_results.visibility = View.VISIBLE
                }

                booksAdapter.items = books
            } else {
                TransitionManager.beginDelayedTransition(
                    container, getTransition(R.transition.auto)
                )
                progress.visibility = View.GONE
                setNoResultsVisibility(View.VISIBLE)
            }
        })

        val bookPreloadSizeProvider = ViewPreloadSizeProvider<Book>()
        val bookPreloader = RecyclerViewPreloader(this, booksAdapter, bookPreloadSizeProvider, 4)
        val layoutManager = GridLayoutManager(this, BooksAdapter.DEFAULT_COLUMNS_NUMBER)

        search_results.apply {
            this.adapter = booksAdapter
            itemAnimator = SlideInItemAnimator()
            this.layoutManager = layoutManager
            setHasFixedSize(true)
            addOnScrollListener(bookPreloader)
        }
    }

    override fun onEnterAnimationComplete() {
        search_view.requestFocus()
        ImeUtils.showIme(search_view)
    }

    private fun setupTransitions() {
        // grab the position that the search icon transitions in *from*
        // & use it to configure the return transition
        setEnterSharedElementCallback(object : SharedElementCallback() {
            override fun onSharedElementStart(
                sharedElementNames: List<String>,
                sharedElements: List<View>?,
                sharedElementSnapshots: List<View>
            ) {
                if (sharedElements != null && sharedElements.isNotEmpty()) {
                    val searchIcon = sharedElements[0]
                    if (searchIcon.id != R.id.search_back) return
                    val centerX = (searchIcon.left + searchIcon.right) / 2
                    val hideResults = TransitionUtils.findTransition(
                        window.returnTransition as TransitionSet,
                        CircularReveal::class.java, R.id.results_container
                    ) as CircularReveal?
                    hideResults?.setCenter(Point(centerX, 0))
                }
            }
        })
    }

    private fun setupSearchView(query: String?) {
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        search_view.apply {
            setSearchableInfo(searchManager.getSearchableInfo(componentName))
            // hint, inputType & ime options seem to be ignored from XML! Set in code
            queryHint = getString(R.string.search_hint)
            inputType = InputType.TYPE_TEXT_FLAG_CAP_WORDS
            imeOptions = imeOptions or EditorInfo.IME_ACTION_SEARCH or
                    EditorInfo.IME_FLAG_NO_EXTRACT_UI or EditorInfo.IME_FLAG_NO_FULLSCREEN
            setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String): Boolean {
                    searchFor(query)
                    return true
                }

                override fun onQueryTextChange(query: String): Boolean {
                    if (query.isEmpty()) {
                        clearResults()
                    }
                    return true
                }
            })
            query?.let {
                setQuery(it, true)
            }
        }
    }

    internal fun searchFor(query: String) {
        clearResults()
        progress.visibility = View.VISIBLE
        ImeUtils.hideIme(search_view)
        search_view.clearFocus()
        viewModel.searchFor(query)
    }

    internal fun clearResults() {
        TransitionManager.beginDelayedTransition(container, getTransition(R.transition.auto))
        booksAdapter.items = emptyList()
        search_results.visibility = View.GONE
        progress.visibility = View.GONE
        results_scrim.visibility = View.GONE
        setNoResultsVisibility(View.GONE)
    }

    private fun setNoResultsVisibility(visibility: Int) {
        noResults = noResults
            ?: (findViewById<View>(R.id.stub_no_search_results) as ViewStub).inflate() as TextView

        if (visibility == View.VISIBLE) {
            noResults?.apply {
                setOnClickListener {
                    search_view.setQuery("", false)
                    search_view.requestFocus()
                    ImeUtils.showIme(search_view)
                }
            }
            val message = String.format(
                getString(R.string.no_search_results), search_view.query.toString()
            ).toSpannable()
            message[message.indexOf('“') + 1, message.length - 1] = StyleSpan(Typeface.ITALIC)

            noResults?.text = message
        }
        noResults?.visibility = visibility
    }

    private fun getTransition(@TransitionRes transitionId: Int): Transition? {
        var transition: Transition? = transitions.get(transitionId)
        if (transition == null) {
            transition = TransitionInflater.from(this).inflateTransition(transitionId)
            transitions.put(transitionId, transition)
        }
        return transition
    }

    private fun dismiss() {
        // clear the background else the touch ripple moves with the translation which looks bad
        search_back.background = null
        finishAfterTransition()
    }

    override fun onBackPressed() {
        dismiss()
    }

    override fun onPause() {
        // needed to suppress the default window animation when closing the activity
        overridePendingTransition(0, 0)
        super.onPause()
    }
}
