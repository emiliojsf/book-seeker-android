package com.ciandt.book.seeker.shared.persistence.domain.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "last_searches")
data class LastSearch(
    @PrimaryKey val query: String,
    var updatedAt: Date
)