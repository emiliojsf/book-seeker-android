package com.ciandt.book.seeker.shared.common.model

import android.os.Parcelable
import com.ciandt.book.seeker.extensions.DbBook
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class Book(
    val trackId: Long,
    val trackName: String? = null,
    val trackCensoredName: String? = null,
    val description: String? = null,
    val artistName: String? = null,
    val genres: List<String>? = null,
    val artistViewUrl: String? = null,
    val trackViewUrl: String? = null,
    val artworkUrl60: String? = null,
    val artworkUrl100: String? = null,
    val formattedPrice: String? = null,
    val releaseDate: String? = null,
    val averageUserRating: Double? = null,
    val userRatingCount: Int? = null
) : Parcelable

fun Book.toDatabaseModel(): DbBook {
    return DbBook(
        trackId,
        trackName,
        trackCensoredName,
        description,
        artistName,
        genres,
        artistViewUrl,
        trackViewUrl,
        artworkUrl60,
        artworkUrl100,
        formattedPrice,
        releaseDate,
        averageUserRating,
        userRatingCount,
        Date()
    )
}