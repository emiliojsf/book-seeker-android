package com.ciandt.book.seeker.shared.persistence.di

import androidx.room.Room
import com.ciandt.book.seeker.shared.persistence.BooksDatabase
import com.ciandt.book.seeker.shared.persistence.LastSearchesDatabase
import com.ciandt.book.seeker.shared.persistence.data.repository.BookRepository
import com.ciandt.book.seeker.shared.persistence.data.repository.LastSearchRepository
import com.ciandt.book.seeker.shared.persistence.domain.repository.IBookRepository
import com.ciandt.book.seeker.shared.persistence.domain.repository.ILastSearchRepository
import org.koin.dsl.module

val persistenceModule = module {
    single { Room.databaseBuilder(get(), BooksDatabase::class.java, "books").build() }

    single {
        Room.databaseBuilder(get(), LastSearchesDatabase::class.java, "last_searches").build()
    }

    single { get<BooksDatabase>().bookDao() }

    single { get<LastSearchesDatabase>().lastSearchDao() }

    single<IBookRepository> { BookRepository(get()) }

    single<ILastSearchRepository> { LastSearchRepository(get()) }
}