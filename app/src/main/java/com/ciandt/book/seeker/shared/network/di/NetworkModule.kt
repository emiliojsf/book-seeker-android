package com.ciandt.book.seeker.shared.network.di

import com.ciandt.book.seeker.BuildConfig
import com.ciandt.book.seeker.shared.network.remote.MyApi
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

private const val CONNECT_TIMEOUT = 15L
private const val WRITE_TIMEOUT = 30L
private const val READ_TIMEOUT = 30L

val networkModule = module {
    factory { Cache(androidApplication().cacheDir, 10L * 1024 * 1024) }
    factory { provideOkHttpClient(get(), provideInterceptor()) }
    factory { provideApi(get()) }
    single { provideRetrofit(get()) }
}

private fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder()
        .baseUrl(BuildConfig.DATA_API_URL)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
}

private fun provideOkHttpClient(cache: Cache, interceptor: HttpLoggingInterceptor): OkHttpClient {
    return OkHttpClient.Builder().apply {
        cache(cache)
        connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
        writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
        readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
        addInterceptor(interceptor)
        retryOnConnectionFailure(true)
    }.build()
}

private fun provideApi(retrofit: Retrofit) = retrofit.create(MyApi::class.java)

private fun provideInterceptor() =
    HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BASIC }