package com.ciandt.book.seeker.bookdetails.di

import com.ciandt.book.seeker.bookdetails.data.repository.BookDetailsRepository
import com.ciandt.book.seeker.bookdetails.data.source.BookDetailsDataSource
import com.ciandt.book.seeker.bookdetails.domain.repository.IBookDetailsRepository
import com.ciandt.book.seeker.bookdetails.viewmodel.BookDetailsViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val bookDetailsModule = module {
    single<IBookDetailsRepository> { BookDetailsRepository(get()) }
    single { BookDetailsDataSource(get()) }
    viewModel { BookDetailsViewModel(get(), get()) }
}