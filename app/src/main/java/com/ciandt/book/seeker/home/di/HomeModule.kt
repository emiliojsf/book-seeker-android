package com.ciandt.book.seeker.home.di

import com.ciandt.book.seeker.home.viewmodel.HomeViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val homeModule = module {

    viewModel { HomeViewModel(get(), get()) }

}