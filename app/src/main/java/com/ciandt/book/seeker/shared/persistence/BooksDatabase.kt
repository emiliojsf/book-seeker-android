package com.ciandt.book.seeker.shared.persistence

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.ciandt.book.seeker.shared.persistence.converter.DateConverter
import com.ciandt.book.seeker.shared.persistence.converter.GenreListConverter
import com.ciandt.book.seeker.shared.persistence.domain.dao.BookDao
import com.ciandt.book.seeker.shared.persistence.domain.model.Book

/**
 * The Room database that contains the Books table
 */
@Database(entities = [Book::class], version = 1)
@TypeConverters(DateConverter::class, GenreListConverter::class)
abstract class BooksDatabase : RoomDatabase() {

    abstract fun bookDao(): BookDao
}