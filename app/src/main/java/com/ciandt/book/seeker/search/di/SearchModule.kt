package com.ciandt.book.seeker.search.di

import com.ciandt.book.seeker.search.data.repository.BooksRepository
import com.ciandt.book.seeker.search.data.source.SearchRemoteDataSource
import com.ciandt.book.seeker.search.domain.repository.IBooksRepository
import com.ciandt.book.seeker.search.viewmodel.SearchViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val searchModule = module {
    single { SearchRemoteDataSource(get()) }
    single<IBooksRepository> { BooksRepository(get()) }
    viewModel { SearchViewModel(get(), get()) }
}