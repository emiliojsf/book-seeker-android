package com.ciandt.book.seeker.shared.persistence.data.repository

import com.ciandt.book.seeker.shared.persistence.domain.repository.ILastSearchRepository
import com.ciandt.book.seeker.shared.persistence.domain.dao.LastSearchDao
import com.ciandt.book.seeker.shared.persistence.domain.model.LastSearch
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.*

class LastSearchRepository(private val lastSearchDao: LastSearchDao) : ILastSearchRepository {

    companion object {
        private const val LAST_SEARCHES_LIMIT_IN_DATABASE = 5
    }

    override suspend fun insertOrUpdate(lastSearch: String) = withContext(Dispatchers.IO) {
        lastSearchDao.insert(LastSearch(query = lastSearch, updatedAt = Date()))
        deleteExcess()
    }

    override suspend fun getAllLastSearches(): List<LastSearch> = withContext(Dispatchers.IO) {
        return@withContext getAllItemsSorted()
    }

    private suspend fun getAllItemsSorted() = withContext(Dispatchers.IO) {
        return@withContext lastSearchDao.getAll().sortedByDescending { it.updatedAt }
    }

    private suspend fun deleteExcess() = withContext(Dispatchers.IO) {
        val excess = getAllItemsSorted().drop(LAST_SEARCHES_LIMIT_IN_DATABASE)
        lastSearchDao.delete(excess.map { it.query })
    }

}