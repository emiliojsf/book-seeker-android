package com.ciandt.book.seeker.shared.persistence.domain.model

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
@Entity(tableName = "books")
data class Book(
    @PrimaryKey @ColumnInfo(name = "bookid") val id: Long,
    val trackName: String? = null,
    val trackCensoredName: String? = null,
    val description: String? = null,
    val artistName: String? = null,
    val genres: List<String>? = null,
    val artistViewUrl: String? = null,
    val trackViewUrl: String? = null,
    val artworkUrl60: String? = null,
    val artworkUrl100: String? = null,
    val formattedPrice: String? = null,
    val releaseDate: String? = null,
    val averageUserRating: Double? = null,
    val userRatingCount: Int? = null,
    var updatedAt: Date
) : Parcelable