package com.ciandt.book.seeker.bookdetails.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ciandt.book.seeker.bookdetails.domain.repository.IBookDetailsRepository
import com.ciandt.book.seeker.shared.common.model.Book
import com.ciandt.book.seeker.shared.common.model.toDatabaseModel
import com.ciandt.book.seeker.shared.persistence.domain.repository.IBookRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class BookDetailsViewModel(
    private val bookDetailsRepository: IBookDetailsRepository,
    private val bookDatabaseRepository: IBookRepository
) : ViewModel() {

    private val scope = CoroutineScope(Dispatchers.IO)

    val showLoading = MutableLiveData<Boolean>()
    val bookDetails = MutableLiveData<Book>()

    fun getBookDetails(id: Long) {
        scope.launch {
            showLoading.postValue(true)

            val bookUpdated = bookDetailsRepository.getBookDetails(id)
            bookDetails.postValue(bookUpdated)

            bookUpdated?.let {
                saveBookToDatabase(bookUpdated)
            }

            showLoading.postValue(false)
        }
    }

    fun saveBookToDatabase(book: Book) {
        scope.launch {
            bookDatabaseRepository.insertOrUpdate(book.toDatabaseModel())
        }
    }

}