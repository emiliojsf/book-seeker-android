package com.ciandt.book.seeker.bookdetails.domain.repository

import com.ciandt.book.seeker.shared.common.model.Book

interface IBookDetailsRepository {
    suspend fun getBookDetails(id: Long) : Book?
}