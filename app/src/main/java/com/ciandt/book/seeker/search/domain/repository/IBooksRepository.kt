package com.ciandt.book.seeker.search.domain.repository

import com.ciandt.book.seeker.shared.common.model.Book

interface IBooksRepository {
    suspend fun search(query: String): List<Book>?

    fun getBook(id: Long): Book?
}