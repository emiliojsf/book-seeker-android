package com.ciandt.book.seeker.home.viewmodel

import android.annotation.SuppressLint
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ciandt.book.seeker.shared.persistence.domain.repository.IBookRepository
import com.ciandt.book.seeker.shared.persistence.domain.repository.ILastSearchRepository
import com.ciandt.book.seeker.shared.persistence.domain.model.Book
import com.ciandt.book.seeker.shared.persistence.domain.model.LastSearch
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@SuppressLint("CheckResult")
class HomeViewModel(
    private val bookRepository: IBookRepository,
    private val lastSearchRepository: ILastSearchRepository
) : ViewModel() {

    val books = MutableLiveData<List<Book>>()
    val lastSearches = MutableLiveData<List<LastSearch>>()
    private val scope = CoroutineScope(Dispatchers.IO)

    fun getAllBooks() {
        scope.launch {
            books.postValue(bookRepository.getAllBooks())
        }
    }

    fun getAllLastSearches() {
        scope.launch {
            lastSearches.postValue(lastSearchRepository.getAllLastSearches())
        }
    }
}