package com.ciandt.book.seeker

import com.ciandt.book.seeker.bookdetails.di.bookDetailsModule
import com.ciandt.book.seeker.home.di.homeModule
import com.ciandt.book.seeker.shared.network.di.networkModule
import com.ciandt.book.seeker.shared.persistence.di.persistenceModule
import com.ciandt.book.seeker.search.di.searchModule
import org.koin.dsl.module

val applicationModule = module {
}

val koinModules = listOf(
    applicationModule,
    networkModule,
    persistenceModule,
    homeModule,
    searchModule,
    bookDetailsModule
)