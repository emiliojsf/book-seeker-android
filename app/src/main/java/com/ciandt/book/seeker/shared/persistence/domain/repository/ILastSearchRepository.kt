package com.ciandt.book.seeker.shared.persistence.domain.repository

import com.ciandt.book.seeker.shared.persistence.domain.model.LastSearch

interface ILastSearchRepository {

    suspend fun insertOrUpdate(lastSearch: String)

    suspend fun getAllLastSearches(): List<LastSearch>

}