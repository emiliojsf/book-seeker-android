package com.ciandt.book.seeker.shared.network.domain.model

data class SearchResponse<T>(
    val resultCount: Int,
    val results: List<T>
)