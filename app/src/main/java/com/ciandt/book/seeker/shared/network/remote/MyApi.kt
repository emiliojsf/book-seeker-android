package com.ciandt.book.seeker.shared.network.remote

import com.ciandt.book.seeker.shared.network.domain.model.SearchResponse
import com.ciandt.book.seeker.shared.common.model.Book
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface MyApi {
    @GET("search")
    suspend fun search(
        @Query("term") query: String,
        @Query("entity") entity: String = "ibook"
    ): Response<SearchResponse<Book>>

    @GET("lookup")
    suspend fun lookup(
        @Query("id") id: Long,
        @Query("entity") entity: String = "ibook"
    ): Response<SearchResponse<Book>>
}