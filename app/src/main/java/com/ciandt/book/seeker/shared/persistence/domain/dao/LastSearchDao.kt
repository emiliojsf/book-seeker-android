package com.ciandt.book.seeker.shared.persistence.domain.dao

import androidx.room.*
import com.ciandt.book.seeker.shared.persistence.domain.model.LastSearch

@Dao
interface LastSearchDao {

    /**
     * Get all last searches.
     * @return the books from the table.
     */
    @Query("SELECT * FROM last_searches")
    fun getAll(): List<LastSearch>

    /**
     * Get a last search by id.
     * @return the last search from the table with a specific id.
     */
    @Query("SELECT * FROM last_searches WHERE `query` = :id")
    fun getLastSearchById(id: String): LastSearch?

    /**
     * Insert a last search in the database. If the last search already exists, replace it.
     * @param lastSearch the book to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(lastSearch: LastSearch)

    /**
     * Delete a last search from the database.
     * @param idList the last searches to be deleted.
     */
    @Query("DELETE FROM last_searches WHERE `query` IN (:idList)")
    fun delete(idList: List<String>)
}